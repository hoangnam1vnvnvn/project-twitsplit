import React, { Component } from 'react';
import Login from './login';

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: [],
            name: '',
            login: false,
            error_message: ''
        };
    }

    handleLogin (name) {
        this.setState({
            name: name,
            login: true
        })
    }

    handleNumberMessage (totalMs) {
        let arrayNumber = [];
        let title = "";
        for (let i = 1; i <= totalMs; i++) {
           title = i + '/' + totalMs;
           arrayNumber.push(title);
        }
        return arrayNumber;
    }

    splitMessage (message) {
        if (message) {
            let lengthMs = message.length;
            let arrayMss = this.state.message;
            let error_message = this.state.error_message;
            if (lengthMs <= 50) {
                arrayMss.push(message);
                if (error_message) {
                    this.setState({
                        error_message: ''
                    })
                }
            } else {
                if (message.indexOf(' ') < 0) {
                    if (!error_message) {
                        this.setState({
                            error_message: "Tin nhắn không hợp lệ"
                        })
                    }
                } else {
                    let totalSplitMs =Math.ceil(lengthMs/50);
                    let numberBefore = this.handleNumberMessage(totalSplitMs);
                    let positionWS = 0;
                    for (let i = 0; i < totalSplitMs; i++) {
                        let tempMs = '';
                        let ms = '';
                        let tempPosition = 0;
                        if (positionWS === 0) {
                            tempMs = message.slice(0, 47);
                            positionWS = tempMs.lastIndexOf(' ');
                            ms = numberBefore[i] + ' ' + message.slice(0, positionWS);
                        } else if (i !== totalSplitMs - 1) {
                            tempMs = message.slice(positionWS, positionWS + 47);
                            tempPosition = positionWS;
                            positionWS = tempMs.lastIndexOf(' ') + tempPosition;
                            ms = numberBefore[i] + message.slice(tempPosition, positionWS);
                        } else {
                            ms = numberBefore[i] + message.slice(positionWS, lengthMs);
                        }
                        arrayMss.push(ms);
                    }
                    if (error_message) {
                        this.setState({
                            error_message: ''
                        })
                    }
                }
            }
            this.setState({
                message: arrayMss
            });
            document.getElementById("message_text").value = '';
        }
    }

    sendMessage () {
        let message = document.getElementById("message_text").value;
        this.splitMessage(message);
    }

    onKeyPressSend(e) {
        if (e.keyCode === 13) {
            let message = document.getElementById("message_text").value;
            this.splitMessage(message);
        }
    }


    render() {
        let message = this.state.message ? this.state.message : [];
        let name = this.state.name ? this.state.name : '';
        let error_message = this.state.error_message ? this.state.error_message : '';
        return (
            <div>
                <div className="container">
                    <h1>MY PROJECT</h1>
                    <div className="chat-box" style={{maxWidth: '320px'}}>
                        {!this.state.login ?
                            <Login handleLogin={this.handleLogin.bind(this)}/>
                            :
                            <div className="chat-box--container" style={{padding: '0.75rem', border: '1px solid #dddd'}}>
                                <h4>Xin chào {name}</h4>
                                <div className="chat-box--content" style={{padding: '0.75rem', border: '1px solid #dddd', marginBottom: '0.75rem', maxHeight: '320px', overflow: 'hidden', overflowY: 'auto'}}>
                                    <ul className="list-group">
                                        {message.length > 0 && message.map((item, id) => {
                                            return (
                                                <li className="list-group-item align-right" style={{margin: '0.75rem 0', border: 'none', padding: 0}}>
                                                    <div className="display-name" style={{ color: '#003399', fontWeight: 'bold' }}>{name}</div>
                                                    <p className="mb-0">{item}</p>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                <div className="input-group">
                                    <input type="text" id="message_text" className="form-control" aria-label="With textarea" row={3} defaultValue={""} onKeyDown={this.onKeyPressSend.bind(this)} />
                                    <div className="input-group-prepend">
                                        <a type="submit" className="btn btn-primary" onClick={this.sendMessage.bind(this)}>Gửi</a>
                                    </div>
                                </div>
                                {
                                    error_message && <p>{error_message}</p>
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Message;

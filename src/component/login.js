import React, { Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            error_name: ''
        };
    }

    loginWithName () {
        let name = document.getElementById('name_login').value;
        let sp_key = /[`~!@#$%^&*()-+=+:;"'<>?/,.{}[]|\]/;
        let space = name.indexOf("  ");
        if (name === '' || name.length > 40 || space !== -1 || sp_key.test(name) === true) {
            if (name === '') {
                this.setState({
                    error_name: "Tên không được để trống"
                });

            } else if (name.length > 40) {
                this.setState({
                    error_name: "Tên không được quá 40 ký tự"
                });
            }
            else if (sp_key.test(name) === true) {
                this.setState({
                    error_name: "Vui lòng không sử dụng ký tự đặc biệt"
                });
            } else if (space !== -1) {
                this.setState({
                    error_name: "Tên không được chứa nhiều khoảng trắng liền kề nhau"
                });
            }
            return false;
        } else {
            this.props.handleLogin(name);
        }
    }

    onKeyPressLogin(e) {
        if (e.keyCode === 13) {
            this.loginWithName();
        }
    }

    render() {
        let error = this.state.error_name ? this.state.error_name : '';
        return (
            <div className="chat-box--start" style={{padding: '0.75rem', border: '1px solid #dddd', marginBottom: '24px'}}>
                <h4>Vui lòng nhập tên</h4>
                <div className="form-group">
                    <input type="text" className="form-control" id="name_login" placeholder="nhập tên" onKeyDown={this.onKeyPressLogin.bind(this)} />
                    {error ? 
                        <p id="emailHelp" className="form-text text-muted">{error}</p>
                        : 
                        ''
                    }
                </div>
            </div>
        );
    }
}

export default Login;
